export interface ICats {
  id: number;
  name: string;
  age: number;
}

export let catsArray = [
  {
    id: 1,
    name: 'Rajiv',
    age: 11,
  },
  {
    id: 2,
    name: 'Faizan',
    age: 5,
  },
  {
    id: 3,
    name: 'Ravi',
    age: 9,
  },
  {
    id: 4,
    name: 'Amit',
    age: 8,
  },
];

export const setCatsArray = (newCatsArray: ICats[]) =>
  (catsArray = newCatsArray);
