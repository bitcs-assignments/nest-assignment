import { HttpException, Injectable } from '@nestjs/common';
import { CatEntity } from 'src/db/cat.entity';
import { getRepository } from 'typeorm';

import { ICats, catsArray, setCatsArray } from './constants';
import { validateCatEntry, validateCatID } from './joi.validators';

@Injectable()
export class CatsService {
  async createCat(name: string, age: number): Promise<ICats> {
    const { error } = validateCatEntry({ name, age });
    if (error) throw new HttpException(error.message, 400);

    const cat = new CatEntity();
    [cat.name, cat.age] = [name, age];
    await cat.save();

    return cat;
  }

  async readCats(): Promise<ICats[]> {
    let cats = await getRepository(CatEntity)
      .createQueryBuilder('cats')
      .getMany();
    return cats;
  }

  async updateCat(id: number, name: string, age: number): Promise<ICats> {
    const cat = await CatEntity.findOne(id);
    if (cat) {
      const { error } = validateCatID({ id });
      if (error) throw new HttpException(error.message, 400);

      [cat.name, cat.age] = [name, age];
      await cat.save();

      return cat;
    }
    throw new HttpException("id doesn't exist", 404);
  }

  async deleteCat(id: number): Promise<object> {
    const cat = await CatEntity.findOne(id);
    if (cat) {
      const { error } = validateCatID({ id });
      if (error) throw new HttpException(error.message, 400);

      await CatEntity.delete(id);
      return { message: 'Deleted Successfully' };
    }
    throw new HttpException("id doesn't exist", 404);
  }
}
