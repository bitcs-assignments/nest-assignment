import * as Joi from 'joi';
import { ValidationResult } from 'joi';

export const validateCatEntry = ({ name, age }): ValidationResult => {
  const CatSchema = Joi.object({
    name: Joi.string().min(3).max(100).required(),
    age: Joi.number().min(1).max(15).required(),
  });

  return CatSchema.validate({ name, age });
};

export const validateCatID = ({ id }): ValidationResult => {
  const CatSchema = Joi.object({
    id: Joi.number().min(1).required(),
  });

  return CatSchema.validate({ id });
};
