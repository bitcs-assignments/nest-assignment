import * as Fs from 'fs';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';
import { CatEntity } from './db/cat.entity';
import { catsArray } from './cats/constants';

(async function () {
  try {
    Fs.unlinkSync('src/db/data.db');
  } catch (e) {}

  await NestFactory.createApplicationContext(AppModule);
  catsArray.forEach(async (catsElem) => {
    const cat = new CatEntity();
    for (let key in catsElem) cat[key] = catsElem[key];
    await cat.save();
  });
})();
