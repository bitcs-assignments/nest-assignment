import { Injectable } from '@nestjs/common';

import { ICats, catsArray, setCatsArray } from './constants';

@Injectable()
export class CatsService {
  createCat(catName: string): ICats[] {
    catsArray.push({ id: catsArray.length + 1, name: catName });
    return catsArray;
  }

  readCats(): ICats[] {
    return catsArray;
  }

  updateCat(catID: number, catName: string): ICats[] {
    const newCatsArray = catsArray.map((cat) => {
      if (cat.id === catID) cat.name = catName;
      return cat;
    });
    setCatsArray(newCatsArray);
    return catsArray;
  }

  deleteCat(catID: number): ICats[] {
    setCatsArray(catsArray.filter((cat) => cat.id !== catID));
    return catsArray;
  }
}
