import { Body, Controller, Delete, Get, Post, Put } from '@nestjs/common';

import { CatsService } from './cats.service';
import { ICats } from './constants';

@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  // Create
  @Post()
  createCat(@Body('name') catName: string): ICats[] {
    return this.catsService.createCat(catName);
  }

  // Read
  @Get()
  readCats(): ICats[] {
    return this.catsService.readCats();
  }

  // Update
  @Put()
  updateCat(@Body('id') catID: number, @Body('name') catName: string): ICats[] {
    return this.catsService.updateCat(catID, catName);
  }

  // Delete
  @Delete()
  deleteCat(@Body('id') catID: number): ICats[] {
    return this.catsService.deleteCat(catID);
  }
}
