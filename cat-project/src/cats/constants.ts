export interface ICats {
  id: number;
  name: string;
}

// export type ICats = {
//   id: number;
//   name: string;
// }

export let catsArray = [
  {
    id: 1,
    name: 'Rajiv',
  },
  {
    id: 2,
    name: 'Faizan',
  },
  {
    id: 3,
    name: 'Ravi',
  },
  {
    id: 4,
    name: 'Amit',
  },
];

export const setCatsArray = (newCatsArray: ICats[]) =>
  (catsArray = newCatsArray);
