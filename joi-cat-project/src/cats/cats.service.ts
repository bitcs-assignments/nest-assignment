import { HttpException, Injectable } from '@nestjs/common';

import { ICats, catsArray, setCatsArray } from './constants';
import { validateCatEntry, validateCatID } from './joi.validators';

@Injectable()
export class CatsService {
  createCat(name: string, age: number): ICats[] {
    const { error } = validateCatEntry({ name, age });
    if (error) throw new HttpException(error.message, 400);
    catsArray.push({ id: catsArray.length + 1, name, age });
    return catsArray;
  }

  readCats(): ICats[] {
    return catsArray;
  }

  updateCat(id: number, name: string, age: number): ICats[] {
    let flag = true;
    const { error } = validateCatID({ id });
    if (error) throw new HttpException(error.message, 400);
    const newCatsArray = catsArray.map((cat) => {
      if (cat.id === id) {
        const { error } = validateCatEntry({ name, age });
        if (error) throw new HttpException(error.message, 400);
        cat.name = name;
        flag = false;
      }
      return cat;
    });
    if (flag) throw new HttpException("id doesn't exist", 400);
    setCatsArray(newCatsArray);
    return catsArray;
  }

  deleteCat(id: number): ICats[] {
    const { error } = validateCatID({ id });
    if (error) throw new HttpException(error.message, 400);
    setCatsArray(catsArray.filter((cat) => cat.id !== id));
    return catsArray;
  }
}
